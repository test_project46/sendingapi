FROM python:3.11

WORKDIR /app

COPY requirements.txt .

RUN apt update && \
    apt upgrade -y && \
    apt -y install gcc && \
    python3 -m pip install --upgrade pip && \
    pip3 install -r requirements.txt --no-cache-dir

COPY ./src ./
COPY ./entrypoint.sh ./

CMD ["./entrypoint.sh"]