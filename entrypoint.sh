#!/usr/bin/env bash

nohup celery -A sendingapi worker &
python manage.py runserver 0.0.0.0:8000