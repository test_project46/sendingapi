## Сервис уведомлений

Для запуска необходимо в папке src/ создать файл .env со следующими переменными:
```
URL='URL внешнего API'
TOKEN='Токен для досутпа к внешнему API'
```

Запуск осуществляется командой:

```
docker-compose up --build
```

Дополнительных настроек не требуется. После выполнения команды запустятся контейнеры с Django-приложением и Redis.
Приложение будет доступно по адресу:
```
localhost:8002/
```
В БД уже добавлены тестовые пользователи и суперюзер, через админ-панель по адресу: localhost:8002/admin (логин суперюзера: admin, пароль суперюзера: admin) можно добавить клиентов, рассылки и письма.
Добавить новые сущности можно и через API. Документация к API доступна по адресу:
```
localhost:8002/docs
```

Выполнены следующие дополнительные задания:
- 3. Подготовлен docker-compose для запуска всех сервисов проекта одной командой;
- 5. По адресу: localhost:8002/docs доступна страница со Swagger UI и в нём отображается описание разработанного API;
- 9. Организована обработка ошибок и откладывание запросов при неуспехе для последующей повторной отправки.
- 10. Реализован сбор метрик в формате prometheus, метрики доступны по адресу:
```
localhost:8002/metrics
```
Собираются следующие метрики:
```
api_method_called - количество вызовов API-методов;
model_deletes - количество удаленных объектов с разбивкой по моделям (клиенты, письма, рассылки);
model_creates - количество созданных объектов с разбивкой по моделям (клиенты, письма, рассылки);
external_api_called - количество вызовов внешнего API;
external_api_call_errors - количество ошибок при вызове внешнего API;
```
- 12. Обеспечено подробное логирование на всех этапах обработки запросов.