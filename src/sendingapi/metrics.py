
import prometheus_client
import django_prometheus


prometheus_client.REGISTRY.unregister(django_prometheus.models.model_deletes)
prometheus_client.REGISTRY.unregister(django_prometheus.models.model_inserts)
prometheus_client.REGISTRY.unregister(django_prometheus.models.model_updates)


class Metrics:
    api_method_called = prometheus_client.Counter(
        'api_method_called',
        'total number of api method called',
        ["method"]
    )
    model_deletes = prometheus_client.Counter(
        'model_deletes',
        'total number of model objects deleted',
        ["model"]
    )
    model_creates = prometheus_client.Counter(
        'model_creates',
        'total number of model objects created',
        ["model"]
    )
    external_api_called = prometheus_client.Counter(
        'external_api_called',
        'total number of external API called'
    )
    external_api_call_errors = prometheus_client.Counter(
        'external_api_call_errors',
        'total number of failed external API calls'
    )
