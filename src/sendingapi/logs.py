import sys
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.propagate = False

handler = logging.StreamHandler(stream=sys.stdout)
formatter = logging.Formatter(
    '%(asctime)s [%(levelname)s] %(filename)s, '
    'line %(lineno)d, in %(funcName)s, '
    'message: %(message)s, arguments: %(arguments)s'
)

handler.setFormatter(formatter)
logger.addHandler(handler)
