from __future__ import absolute_import

import os
import celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sendingapi.settings')
app = celery.Celery('sendingapi')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
