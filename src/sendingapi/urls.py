import django.contrib
import django.urls

from . import docs


urlpatterns = [
    django.urls.path(
        'docs/',
        docs.schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'
    ),
    django.urls.path('admin/', django.contrib.admin.site.urls),
    django.urls.path('api/', django.urls.include('api.urls')),
    django.urls.path('', django.urls.include('django_prometheus.urls')),
]
