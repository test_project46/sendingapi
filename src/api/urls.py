import rest_framework.routers
import django.urls

from . import views


router = rest_framework.routers.DefaultRouter()
router.register('client', views.ClientViewSet)
router.register('mailing', views.MailingViewSet)
router.register('message', views.MessageViewSet)

urlpatterns = [
    django.urls.path('', django.urls.include(router.urls)),
]
