import django.shortcuts
import rest_framework
import rest_framework.viewsets
import rest_framework.decorators
import rest_framework.response

from . import serializers
from . import models


class ClientViewSet(rest_framework.viewsets.ModelViewSet):
    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer


class MailingViewSet(rest_framework.viewsets.ModelViewSet):
    queryset = models.Mailing.objects.all()
    serializer_class = serializers.MailingSerializer

    @rest_framework.decorators.action(detail=True, methods=['get'])
    def details(self, request, pk):
        queryset = models.Mailing.objects.all()
        django.shortcuts.get_object_or_404(queryset, pk=pk)
        queryset = models.Message.objects.filter(mailing_id=pk).all()
        serializer = serializers.MessageSerializer(queryset, many=True)
        return rest_framework.response.Response(serializer.data)

    @rest_framework.decorators.action(detail=False, methods=['get'])
    def total(self, request):
        total = models.Mailing.objects.count()
        mailings = models.Mailing.objects.values('id')
        details = {}
        for mailing in mailings:
            amount = models.Message.objects.filter(mailing_id=mailing['id']).all()
            details[mailing['id']] = {
                'Всего писем': amount.count(),
                'Доставлено': amount.filter(status='sent').count(),
                'Не доставлено': amount.filter(status='not sent').count()
            }
        content = {'Всего рассылок': total, 'Письма': details}
        return rest_framework.response.Response(content)


class MessageViewSet(rest_framework.viewsets.ModelViewSet):
    queryset = models.Message.objects.all()
    serializer_class = serializers.MessageSerializer
