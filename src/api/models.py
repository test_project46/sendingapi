import pytz
import django.core.validators
import django.db
import django.utils
from django.db.models import Q, F


class Client(django.db.models.Model):
    TIMEZONE_CHOICES = zip(pytz.all_timezones, pytz.all_timezones)
    phone_validator = django.core.validators.RegexValidator(
        regex=r'^7\d{10}$',
        message="Номер должен быть в формате 7XXXXXXXXXX, Х - цифра от 0 до 9"
    )

    phone = django.db.models.CharField(
        verbose_name='Номер телефона',
        validators=[phone_validator],
        max_length=11,
        unique=True
    )
    code = django.db.models.CharField(
        verbose_name='Код мобильного оператора',
        max_length=3
    )
    tag = django.db.models.CharField(
        verbose_name='Тег рассылки',
        max_length=100,
        blank=True
    )
    timezone = django.db.models.CharField(
        verbose_name='Часовой пояс',
        max_length=50,
        default='UTC',
        choices=TIMEZONE_CHOICES
    )

    def __str__(self):
        return f'Клиент: {self.id}, номер: {self.phone}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Mailing(django.db.models.Model):
    start_date = django.db.models.DateTimeField(
        verbose_name='Начало рассылки'
    )
    stop_date = django.db.models.DateTimeField(
        verbose_name='Конец рассылки'
    )
    text = django.db.models.TextField(
        max_length=255,
        verbose_name='Текст сообщения'
    )
    code = django.db.models.CharField(
        verbose_name='Код мобильного оператора',
        max_length=3
    )
    tag = django.db.models.CharField(
        verbose_name='Тег рассылки',
        max_length=100,
        blank=True
    )

    @property
    def sending(self):
        now = django.utils.timezone.now()
        if self.start_date <= now <= self.stop_date:
            return True
        else:
            return False

    def __str__(self):
        return (f'Рассылка: {self.id}, '
                f'начало: {self.start_date}, '
                f'конец: {self.stop_date}')

    class Meta:
        constraints = [
            django.db.models.CheckConstraint(
                check=Q(start_date__lt=F('stop_date')),
                name='start_date_before_stop')
        ]
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Message(django.db.models.Model):
    STATUS_CHOICES = [
        ("sent", "Отправлено"),
        ("not sent", "Не отправлено"),
    ]

    created = django.db.models.DateTimeField(
        verbose_name='Время создания',
        auto_now_add=True
    )
    status = django.db.models.CharField(
        verbose_name='Статус отправки',
        max_length=20,
        choices=STATUS_CHOICES
    )
    mailing = django.db.models.ForeignKey(
        Mailing,
        on_delete=django.db.models.CASCADE,
        related_name='messages'
    )
    client = django.db.models.ForeignKey(
        Client,
        on_delete=django.db.models.CASCADE,
        related_name='messages'
    )

    def __str__(self):
        return (f'Рассылка: {self.mailing}, '
                f'клиент:{self.client}, '
                f'статус: {self.status}')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
