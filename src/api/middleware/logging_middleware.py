import sendingapi.logs
import sendingapi.metrics


class LoggingMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        arguments = {
            "method": request.method,
            "path": request.path,
            "response_status": response.status_code
        }
        if request.path.startswith('/api/'):
            sendingapi.metrics.Metrics.api_method_called.labels(request.path).inc()
            sendingapi.logs.logger.info(
                'API method called',
                extra={"arguments": arguments}
            )
        return response
