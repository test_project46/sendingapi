import django.apps


class ApiConfig(django.apps.AppConfig):
    name = 'api'

    def ready(self):
        from . import signals
