import rest_framework

from . import models


class ClientSerializer(rest_framework.serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = "__all__"


class MailingSerializer(rest_framework.serializers.ModelSerializer):
    class Meta:
        model = models.Mailing
        fields = "__all__"


class MessageSerializer(rest_framework.serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = '__all__'
