import django.contrib

from . import models


class ClientAdmin(django.contrib.admin.ModelAdmin):
    list_display = ('id', 'phone', 'code', 'tag', 'timezone')
    search_fields = ('phone_number',)
    list_filter = ('id',)


class MailingAdmin(django.contrib.admin.ModelAdmin):
    list_display = ('id', 'start_date', 'stop_date', 'text', 'code', 'tag')
    search_fields = ('text',)
    list_filter = ('id',)


class MessageAdmin(django.contrib.admin.ModelAdmin):
    list_display = ('id', 'created', 'status', 'mailing', 'client')
    search_fields = ('mailing',)
    list_filter = ('id',)


django.contrib.admin.site.register(models.Client, ClientAdmin)
django.contrib.admin.site.register(models.Mailing, MailingAdmin)
django.contrib.admin.site.register(models.Message, MessageAdmin)
