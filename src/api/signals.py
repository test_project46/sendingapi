import django.db.models
from django.db.models import Q
import django.db.models.signals
import django.dispatch
import django.db.models.signals
from django.db import transaction
import sendingapi.logs
import django.forms.models
import sendingapi.metrics
import django.utils

from . import models
from . import tasks


@django.dispatch.receiver(
    django.db.models.signals.post_save,
    sender=models.Mailing,
    dispatch_uid="create_message"
)
def create_message(sender, instance, created, **kwargs):
    if created:
        sendingapi.metrics.Metrics.model_creates.labels('Mailing').inc()
        sendingapi.logs.logger.info(
            'Mailing created',
            extra={
                "arguments": django.forms.models.model_to_dict(instance)
            }
        )
        mailing = models.Mailing.objects.filter(id=instance.id).first()
        if mailing.stop_date < django.utils.timezone.now():
            sendingapi.logs.logger.info(
                'Mailing expired',
                extra={
                    "arguments": django.forms.models.model_to_dict(instance)
                }
            )
            return

        clients = models.Client.objects.filter(
            Q(code=mailing.code) | Q(tag=mailing.tag)
        ).all()

        for client in clients:
            message = models.Message.objects.create(
                status="not sent",
                client_id=client.id,
                mailing_id=instance.id
            )
            message.save()

            data = {
                "message_id": message.id,
                "phone": client.phone,
                "text": mailing.text,
                "mailing_id": mailing.id,
                "client_id": client.id
            }

            if instance.sending:
                transaction.on_commit(lambda: tasks.send_message.apply_async(
                    (data,),
                    expires=mailing.stop_date
                ))
            else:
                sendingapi.logs.logger.info(
                    f'Message added to queue, start: {mailing.start_date}',
                    extra={'arguments': data}
                )
                transaction.on_commit(lambda: tasks.send_message.apply_async(
                    args=(data,),
                    eta=mailing.start_date,
                    expires=mailing.stop_date
                ))


@django.dispatch.receiver(
    django.db.models.signals.post_delete,
    sender=models.Mailing,
    dispatch_uid="delete_mailing_log"
)
def get_delete_mailing_log(sender, instance, **kwargs):
    sendingapi.metrics.Metrics.model_deletes.labels('Mailing').inc()
    sendingapi.logs.logger.info(
        'Mailing deleted',
        extra={
            "arguments": django.forms.models.model_to_dict(instance)
        }
    )


@django.dispatch.receiver(
    django.db.models.signals.post_save,
    sender=models.Message,
    dispatch_uid="create_message_log"
)
def get_create_message_log(sender, instance, created, **kwargs):
    if created:
        sendingapi.metrics.Metrics.model_creates.labels('Message').inc()
        sendingapi.logs.logger.info(
            'Message created',
            extra={
                "arguments": django.forms.models.model_to_dict(instance)
            }
        )


@django.dispatch.receiver(
    django.db.models.signals.post_delete,
    sender=models.Message,
    dispatch_uid="delete_message_log"
)
def get_delete_message_log(sender, instance, **kwargs):
    sendingapi.metrics.Metrics.model_deletes.labels('Message').inc()
    sendingapi.logs.logger.info(
        'Message deleted',
        extra={
            "arguments": django.forms.models.model_to_dict(instance)
        }
    )


@django.dispatch.receiver(
    django.db.models.signals.post_save,
    sender=models.Client,
    dispatch_uid="create_client_log"
)
def get_create_client_log(sender, instance, created, **kwargs):
    if created:
        sendingapi.metrics.Metrics.model_creates.labels('Client').inc()
        sendingapi.logs.logger.info(
            'Client created',
            extra={
                "arguments": django.forms.models.model_to_dict(instance)
            }
        )


@django.dispatch.receiver(
    django.db.models.signals.post_delete,
    sender=models.Client,
    dispatch_uid="delete_client_log"
)
def get_delete_client_log(sender, instance, **kwargs):
    sendingapi.metrics.Metrics.model_deletes.labels('Client').inc()
    sendingapi.logs.logger.info(
        'Client deleted',
        extra={
            "arguments": django.forms.models.model_to_dict(instance)
        }
    )
