import requests
import django.conf

import sendingapi.celery
import sendingapi.logs
import sendingapi.metrics
from . import models


@sendingapi.celery.app.task(bind=True, retry_backoff=True)
def send_message(
    self, data,
    url=django.conf.settings.URL, token=django.conf.settings.URL
):
    message_id = data.get('message_id')

    header = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json'}
    try:
        sendingapi.logs.logger.debug(
            'Sending request to external API',
            extra={"arguments": data}
        )
        sendingapi.metrics.Metrics.external_api_called.inc()
        requests.post(url=url + str(message_id), headers=header, json=data)

        sendingapi.logs.logger.info(
            'Message sent',
            extra={"arguments": data}
        )

    except requests.exceptions.RequestException as exc:
        sendingapi.logs.logger.warning(
            'Failed to send message', extra={"arguments": data},
            stack_info=True
        )
        sendingapi.metrics.Metrics.external_api_call_errors.inc()
        raise self.retry(exc=exc)
    else:
        models.Message.objects.filter(pk=message_id).update(status='sent')
